---
title: "À Propos"
date: 2019-04-02T17:29:33Z
draft: false
sharingicons: false
menu: "navbar"
weight: 1
timestamp: false
keywords: ["association","raid","aventure","solidaire","solidarité","étudiants","europraid","cyber","pilots","cyberpilots","205","voiture","enfants","soutien","europe","est","projet","associatif","mission","matériel","scolaire","sportif","écoles","pays","culturel","financement","dons","baptiste moine","marianne millour","florian charbonneau"]
---

Nous sommes les **Cyber'Pilots**, une association à but non lucratif créée par trois étudiants dans le but de participer à l'édition 2019 du **raid-aventure solidaire Europ'Raid**.

L’Europ’Raid est le premier raid-aventure en Europe. C’est un tour d’Europe culturel et solidaire de 10 000 kilomètres à travers 20 pays en 23 jours.

A bord de notre peugeot 205, nous avons pour mission d’**acheminer 70 kilogrammes de matériel scolaire ou sportif** dans des écoles isolées d’Europe de l’Est (Albanie, Bosnie-Herzégovine, ...) pour des **enfants âgés de 5 à 15 ans**.

L'Albanie, par exemple, est l’un des pays les plus pauvres d’Europe : un tiers des enfants vit avec moins de 2 dollars par jour. Les écoles et les centres de santé des villes sont surpeuplés et la majorité de ces établissements ne disposent pas d'outils pédagogiques suffisants.

Afin de de supporter l'ensemble du projet, nous devons développer un plan de financement. Il comprend l'inscription au raid, l’achat et la mise en état de la voiture, l’assurance, l'essence, ...

Vous pouvez nous soutenir sur [Leetchi](https://www.leetchi.com/c/cyberpilots-europraid-2019), tous les dons sont les bienvenus.