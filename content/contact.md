---
title: "Contact"
date: 2019-04-02T17:29:33Z
draft: false
sharingicons: false
menu: "navbar"
weight: 2
timestamp: false
keywords: ["association","raid","aventure","solidaire","solidarité","étudiants","europraid","cyber","pilots","cyberpilots","205","voiture","enfants","soutien","europe","est","projet","associatif","mission","matériel","scolaire","sportif","écoles","pays","culturel","financement","dons","baptiste moine","marianne millour","florian charbonneau"]
---

Envoyez-nous un message en utilisant le formulaire ci-dessous et nous vous recontacterons avec les informations dont vous avez besoin.

<form method="POST" action="https://formspree.io/cyberpilots.europraid@gmail.com">
    <div class="form-group">
        <label for="emailAddress">Email&nbsp;:</label>
        <input type="email" class="form-control" name="email" id="emailAddress" placeholder="Votre email">
    </div>
    <div class="form-group">
        <label for="message">Message&nbsp;:</label>
        <textarea class="form-control" name="message" id="message" placeholder="Votre message" rows="3"></textarea>
    </div>
    <input type="text" name="_gotcha" style="display:none" />
    <button type="submit" class="btn btn-primary">Envoyer</button>
</form>