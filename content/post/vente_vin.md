---
title: "Opération vente de vin"
date: 2019-04-14T21:06:00Z
draft: false
Description: ""
Tags: []
Categories: []
---

Amateurs d'oenologie ou dégustateurs aguerris, nous vous proposons de nous soutenir en participant à notre opération de vente solidaire de cartons de dégustation de vin.

Organisée en partenariat avec l’un des plus anciens domaines viticoles du vignoble Nantais (XVIIe siècle), ces caisses du château de Briacé vous permettent de découvrir une richesse d'aromes avec finesse et d'élégance.

<center><img src="/img/chateau_briace.jpg" alt="Château de Briacé" title="Château de Briacé (Le Landreau)"/></center>

Cette vente de vin est destinée aux particuliers, mais aussi aux entreprises, associations et collectivités désireuses de soutenir notre initiative.

Si vous souhaitez participer, vous trouverez le bon de commande à [cette adressse](https://www.europraid.fr/wp-content/uploads/2018/11/Flyer-vin-2019-min.pdf) ou pouvez nous contacter directement dans la section [contact du site](/contact).

Merci d'avance pour votre soutien !